# Messages and Events

This repo, shows a number of simply global messaging/events/signals type classes. It details their differences in use and design, and in comments discusses briefly their pros and cons for use, maintainance, and likelihood of misuse/unintended error.

This is my no means exhaustive, but covers a range of basic introductory concepts and options. For more full-fledged Unity-centric approaches, see [Unity-Atoms](https://github.com/AdamRamberg/unity-atoms)