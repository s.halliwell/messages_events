﻿namespace SAE
{
    /// <summary>
    /// Signals are built on top of regular c# delegate and event constructs. When you want a new signal,
    /// you need to write more code.
    ///
    /// This demo shows using global static signals but they can be per instance too.
    /// </summary>
    public static class DemoSignals
    {
        //a signal that sends no information similar to Unity's Button.OnClick
        public static System.Action OnDemoTest;

        //a signal that mimics our PostOffice major minor, Major here is backed into the type "Player"
        //  and minor is the string we send with the signal
        public static System.Action<string> OnPlayerNotify;

        //a more specific signal sending multiple params with it for contenxt, you know it only
        //  gets called when the player dies and perhaps the first GO is the player's and the second is the object
        //  that caused it to die, and so on.
        public static System.Action<UnityEngine.GameObject, UnityEngine.GameObject> OnPlayerDeathParams;

        //a specific but context based signal for player death. By declaring an object that holds
        //  all the information related to the signal/event, it is easy to add more information at
        //  a later date, without having to change every line of code that was previously interacting
        //  with the signal. This is how Unity's OnCollisioEnter messages are constructed.
        public class PlayerDeathArgs
        {
            public UnityEngine.GameObject player, killer;
            public float previousHealth;
            public UnityEngine.Vector3 lastSafePosition;
            //...
        }

        public static System.Action<PlayerDeathArgs> OnPlayerDeathContext;
    }
}