﻿namespace Demo
{
    public class EventDelegateBasedDemoDelegates
    {
        public delegate void IntDelegateA(int iVal);

        public delegate void StringDelegateB(string sVal);

        public delegate void MultiParamDelegateC(int iVal, string sVal);

        public delegate void EmptyDelegateD();

        public delegate void EmptyDelegateE();
    }
}