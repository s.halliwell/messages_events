﻿using SAE;
using UnityEngine;

namespace Demo
{
    public class EventDelegateBasedDemoSend : MonoBehaviour
    {
        public UnityEngine.UI.InputField inputFieldA, inputFieldB, inputFieldC;

        public void SendA()
        {
            if (int.TryParse(inputFieldA.text, out int val))
            {
                GlobalEventByDel<EventDelegateBasedDemoDelegates.IntDelegateA>.Delegate?.Invoke(val);
            }
            else
            {
                Debug.LogError("Cannot send A, expects int in text field");
            }
        }

        public void SendB()
        {
            GlobalEventByDel<EventDelegateBasedDemoDelegates.StringDelegateB>.Delegate?.Invoke(inputFieldB.text);
        }

        public void SendC()
        {
            var args = inputFieldC.text.Split(',');
            if (int.TryParse(args[0], out int val))
            {
                GlobalEventByDel<EventDelegateBasedDemoDelegates.MultiParamDelegateC>.Delegate?.Invoke(val, args[1]);
            }
            else
            {
                Debug.LogError("Cannot send C, expects int first in text field");
            }
        }

        public void SendD()
        {
            GlobalEventByDel<EventDelegateBasedDemoDelegates.EmptyDelegateD>.Delegate?.Invoke();
        }

        public void SendE()
        {
            GlobalEventByDel<EventDelegateBasedDemoDelegates.EmptyDelegateE>.Delegate?.Invoke();
        }
    }
}