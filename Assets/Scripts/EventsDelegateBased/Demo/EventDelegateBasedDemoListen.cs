﻿using SAE;
using UnityEngine;

namespace Demo
{
    public class EventDelegateBasedDemoListen : MonoBehaviour
    {
        private void OnEnable()
        {
            GlobalEventByDel<EventDelegateBasedDemoDelegates.IntDelegateA>.Add(IntDelA);
            GlobalEventByDel<EventDelegateBasedDemoDelegates.StringDelegateB>.Add(StringDelB);
            GlobalEventByDel<EventDelegateBasedDemoDelegates.MultiParamDelegateC>.Add(MultiIntStringC);
            GlobalEventByDel<EventDelegateBasedDemoDelegates.EmptyDelegateD>.Add(ActionD);
            GlobalEventByDel<EventDelegateBasedDemoDelegates.EmptyDelegateE>.Add(ActionE);
        }

        private void OnDisable()
        {
            GlobalEventByDel<EventDelegateBasedDemoDelegates.IntDelegateA>.Remove(IntDelA);
            GlobalEventByDel<EventDelegateBasedDemoDelegates.StringDelegateB>.Remove(StringDelB);
            GlobalEventByDel<EventDelegateBasedDemoDelegates.MultiParamDelegateC>.Remove(MultiIntStringC);
            GlobalEventByDel<EventDelegateBasedDemoDelegates.EmptyDelegateD>.Remove(ActionD);
            GlobalEventByDel<EventDelegateBasedDemoDelegates.EmptyDelegateE>.Remove(ActionE);
        }

        private void IntDelA(int a)
        {
            Debug.Log(a);
        }

        private void StringDelB(string s)
        {
            Debug.Log(s);
        }

        private void MultiIntStringC(int a, string b)
        {
            Debug.Log(a.ToString() + " & " + b);
        }

        private void ActionD()
        {
            Debug.Log("Parameterless Delegate D");
        }

        private void ActionE()
        {
            Debug.Log("Parameterless Delegate E");
        }
    }
}