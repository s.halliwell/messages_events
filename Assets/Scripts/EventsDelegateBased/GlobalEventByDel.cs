﻿namespace SAE
{
    //originally from https://forum.unity.com/threads/recommended-event-system.856294/
    /// <summary>
    /// This implementation is very slim, it puts the work on the user to do the hardwiring of
    /// their param list when Invoking the event. This is in addition to using the compiler,
    /// to ensuring statics are all the across the code are same specialisation, most of our event
    /// samples do this.
    ///
    /// These changes in responsibility, mean there are no dictionary lookups making this faster in many
    /// circumstances than most other global event/message systems. Much like the Signals, this relies on
    /// users to make new delegates to create new message channels and much like the EventManager, allows the
    /// user to send any number of type of data through the event as they determine the delegate signature.
    ///
    /// User defines a new delegate, for example;
    ///   public delegate void PlayerDeath(Player p, GameObject deathCause);
    ///
    /// Callers and Listeners then use that delegate name, for example;
    ///   via GlobalEvent<PlayerDeath>
    /// Lets say they have a function
    ///   void MyPlayerDeathHandler(Player p, GameObject deathCause) {}
    /// So to Listen we do
    ///   GlobalEvent<PlayerDeath>.Add(MyPlayerDeathHandler);
    /// and to remove
    ///   GlobalEvent<PlayerDeath>.Remove(MyPlayerDeathHandler);
    /// For someone to call it, they need to use the delegate to complete the binding
    ///   GlobalEvent<PlayerDeath>.Delegate?.Invoke(para, deathCause);
    /// </summary>
    public static class GlobalEventByDel<T> where T : System.Delegate
    {
        public static T Delegate { get; private set; }

        public static void Add(T callback)
        {
            Delegate = System.Delegate.Combine(Delegate, callback) as T;
        }

        public static void Remove(T callback)
        {
            Delegate = System.Delegate.Remove(Delegate, callback) as T;
        }

        public static void Clear()
        {
            Delegate = null;
        }

        public static int ListenerCount
        {
            get
            {
                return Delegate?.GetInvocationList()?.Length ?? 0;
            }
        }
    }
}