﻿using SAE;
using UnityEngine;
using UnityEngine.UI;

namespace Demo
{
    public class PostDemoSend : MonoBehaviour
    {
        public InputField majorText, minorText;

        public void Start()
        {
            majorText.text = "Player";
        }

        public void SendFromTextBoxes()
        {
            PostOffice.Global.Send(majorText.text, minorText.text);
        }

        public void SendDemoSimple()
        {
            PostOffice.Global.Send("Demo", "Simple");
        }

        public void SendDemoNoone()
        {
            PostOffice.Global.Send("Demo", "Noone");
        }
    }
}