﻿using SAE;
using UnityEngine;

namespace Demo
{
    public class PostDemoListen : MonoBehaviour
    {
        public void OnEnable()
        {
            PostOffice.Global.Listen(DemoSimpleHandler, "Demo", "Simple");
            PostOffice.Global.Listen(PlayerAnyHandler, "Player", string.Empty);
        }

        public void OnDisable()
        {
            PostOffice.Global.StopListening(DemoSimpleHandler, "Demo", "Simple");
            PostOffice.Global.Listen(PlayerAnyHandler, "Player", string.Empty);
        }

        public void DemoSimpleHandler(string major, string minor)
        {
            Debug.Log("Received " + major + " : " + minor);
        }

        public void PlayerAnyHandler(string major, string minor)
        {
            Debug.Log("Received " + major + " : " + minor);
        }
    }
}