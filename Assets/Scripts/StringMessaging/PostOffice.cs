﻿using System.Collections.Generic;

namespace SAE
{
    /// <summary>
    /// Messaging system based on sending string tokens to subscribed listeners.
    ///
    /// Support for global and instance messaging.
    /// 
    /// This is very similar in detail to directly using Unity's EventSystem.StartListening and
    /// EventSystem.StopListening 
    /// 
    /// All Listeners must be functions of the signature void(string,string) and are bound using
    /// Listen and StopListening.
    /// </summary>
    public class PostOffice
    {
        public static PostOffice Global = new PostOffice();

        protected class PostBoxInner : Dictionary<string, PostBoxList> { }

        protected class PostBoxList : List<PostOfficeMessageHandler> { }

        protected Dictionary<string, PostBoxInner> lookup = new Dictionary<string, PostBoxInner>();

        public delegate void PostOfficeMessageHandler(string major, string minor);

        public void Listen(PostOfficeMessageHandler listener, string major, string minor)
        {
            //Get or add inner box
            PostBoxInner majorDict;
            if (!lookup.TryGetValue(major, out majorDict))
            {
                majorDict = new PostBoxInner();
                lookup.Add(major, majorDict);
            }

            //get or add inner list
            PostBoxList list;
            if (!majorDict.TryGetValue(minor, out list))
            {
                list = new PostBoxList();
                majorDict.Add(minor, list);
            }

            list.Add(listener);
        }

        public void Send(string major, string minor)
        {
            //get inner dict
            if (lookup.TryGetValue(major, out var majorDict))
            {
                if (!string.IsNullOrEmpty(minor))
                {
                    //get the any and the minor
                    majorDict.TryGetValue(minor, out var minorList);
                    _SendToAll(minorList, major, minor);
                }

                // empty minors get notifed regardless
                majorDict.TryGetValue(string.Empty, out var anyList);
                _SendToAll(anyList, major, minor);
            }
        }

        public void StopListening(PostOfficeMessageHandler listener, string major, string minor)
        {
            //Get or fail
            PostBoxInner majorDict;
            if (!lookup.TryGetValue(major, out majorDict))
            {
                return;
            }

            //get or add inner list
            PostBoxList list;
            if (!majorDict.TryGetValue(minor, out list))
            {
                return;
            }

            list.Remove(listener);
        }

        public void StopListeningAll(PostOfficeMessageHandler listener)
        {
            foreach (var major in lookup)
            {
                foreach (var list in major.Value)
                {
                    list.Value.Remove(listener);
                }
            }
        }

        public void RemoveAllListeners()
        {
            foreach (var major in lookup)
            {
                foreach (var list in major.Value)
                {
                    list.Value.Clear();
                }
            }
        }

        public void RemoveAllMajorListeners(string major)
        {
            //Get or fail
            PostBoxInner majorDict;
            if (!lookup.TryGetValue(major, out majorDict))
            {
                return;
            }

            foreach (var list in majorDict)
            {
                list.Value.Clear();
            }
        }

        public void RemoveAllMinorListeners(string major, string minor)
        {
            //Get or fail
            PostBoxInner majorDict;
            if (!lookup.TryGetValue(major, out majorDict))
            {
                return;
            }

            //get or add inner list
            PostBoxList list;
            if (!majorDict.TryGetValue(minor, out list))
            {
                return;
            }

            list.Clear();
        }

        public int CalculateMajorListeners(string major)
        {
            //Get or fail
            PostBoxInner majorDict;
            if (!lookup.TryGetValue(major, out majorDict))
            {
                return 0;
            }

            int count = 0;

            foreach (var item in majorDict)
            {
                count += item.Value.Count;
            }

            return count;
        }

        public int CalculateMinorListeners(string major, string minor)
        {
            PostBoxInner majorDict;
            if (!lookup.TryGetValue(major, out majorDict))
            {
                return 0;
            }

            //get or add inner list
            PostBoxList list;
            if (!majorDict.TryGetValue(minor, out list))
            {
                return 0;
            }

            return list.Count;
        }

        public int CalculateTotalListeners()
        {
            int count = 0;

            foreach (var major in lookup)
            {
                foreach (var list in major.Value)
                {
                    count += list.Value.Count;
                }
            }

            return count;
        }

        protected static void _SendToAll(List<PostOfficeMessageHandler> list, string major, string minor)
        {
            if (list != null)
            {
                foreach (var item in list)
                {
                    item.Invoke(major, minor);
                }
            }
        }
    }
}