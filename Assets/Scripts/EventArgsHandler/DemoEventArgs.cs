﻿using System;

namespace SAE
{
    /// <summary>
    /// C# has a number of existing event and delegate things;
    /// delegate, event, System.Action, System.Func, eventhandler, and eventargs
    ///
    /// Much of c# winforms is built on binding events to the common function signatures.
    /// Built on top of delegates in the form of EventHandler. Requires object sender to
    /// be cast if it is to be used. Inheriting from EventArgs is recommended and use of
    /// EventArgs.Empty
    ///
    /// These are shown here as globals to match other demos, but more usually are instance
    /// sending 'this' as value of object sender.
    ///
    /// The delegates with object sender, eventargs single param, aligns closely to the pattern
    /// you would see in lower level languages like c and older apis. Where they might be
    /// typedef void (*callBack)(void** object, void* data)
    ///
    /// Unity also has the EventSystems namespace, for the UI, it allows for global string based
    /// callbacks, have a look at https://medium.com/@johntucker_48673/discovering-unity-eventmanager-a040285d0690
    /// for more details there. Unity's UI uses the more full featured Interface and ExecuteEvents
    /// functions found within the same set of classes, you can find more about them here,
    /// https://gist.github.com/stramit/76e53efd67a2e1cf3d2f
    /// </summary>
    public static class DemoEventArgs
    {
        //empty basic EventHandler, which sends object and EventArgs, you can send anything through
        //  them but the generic versions would be prefered as the calling site can then be checked
        //  by the compiler and at the subscription site the programmer can see what the types are in
        //  code rather than having to locate documentation for it.
        public static EventHandler OnDemoTest;

        //possible but bad idea, wants T to inherit from EventArgs
        public static EventHandler<int> OnPlayerNotify;

        //a bundle we can put anything into that inhers from EventArgs. This means the
        //  subscriber can bind their function to generic and the legacy EventHandlers. The user of
        //  EventArgs means code can subscribe and then do an is check to see if the params are
        //  the type they care about before taking action
        public class EventHandlerData : EventArgs
        {
            public int aVal;
        }

        public static EventHandler<EventHandlerData> OnCustomData;
    }
}