﻿using UnityEngine;

namespace Demo
{
    public class EventObjectDemoSend : MonoBehaviour
    {
        public UnityEngine.UI.InputField inputFieldA, inputFieldB;

        public void SendTypeA()
        {
            if (int.TryParse(inputFieldA.text, out int val))
            {
                Demo.GlobalEventObjects.TypeA.aVal = val;
                Demo.GlobalEventObjects.TypeA.Invoke();
            }
            else
            {
                Debug.LogError("Cannot send TypeA, expects int in text field");
            }
        }

        public void SendTypeB()
        {
            Demo.GlobalEventObjects.TypeB.aVal = inputFieldB.text;
            Demo.GlobalEventObjects.TypeB.Invoke();
        }
    }
}