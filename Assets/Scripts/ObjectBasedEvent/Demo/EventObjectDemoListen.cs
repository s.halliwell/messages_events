﻿using UnityEngine;

namespace Demo
{
    public class EventObjectDemoListen : MonoBehaviour
    {
        private void OnEnable()
        {
            Demo.GlobalEventObjects.TypeA.Add(HandlerA);
            Demo.GlobalEventObjects.TypeB.Add(HandlerB);
        }

        private void OnDisable()
        {
            Demo.GlobalEventObjects.TypeA.Remove(HandlerA);
            Demo.GlobalEventObjects.TypeB.Remove(HandlerB);
        }

        private void HandlerA(EventObjectDemoTypeA param)
        {
            Debug.Log(param.aVal);
        }

        private void HandlerB(EventObjectDemoTypeB param)
        {
            Debug.Log(param.aVal);
        }
    }
}