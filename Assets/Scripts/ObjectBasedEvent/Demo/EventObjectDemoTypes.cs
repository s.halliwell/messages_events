﻿namespace Demo
{
    public class EventObjectDemoTypeA : SAE.EventObject<EventObjectDemoTypeA>
    {
        public int aVal;
    }

    public class EventObjectDemoTypeB : SAE.EventObject<EventObjectDemoTypeB>
    {
        public string aVal;
    }

    //we want these global to match other demos
    public static class GlobalEventObjects
    {
        public static EventObjectDemoTypeA TypeA = new EventObjectDemoTypeA();
        public static EventObjectDemoTypeB TypeB = new EventObjectDemoTypeB();
    }
}