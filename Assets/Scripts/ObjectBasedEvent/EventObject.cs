﻿namespace SAE
{
    /// <summary>
    /// This event sends itself as the param to all listeners. It is a mixin of sorts. It may look a bit odd
    /// but it serves a purpose, it gets around a limitation of the Global static that exists in both
    /// the Param and Delegate based event classes, the final type doesn't dictate the static the T does.
    /// With this mixin version, the final type is the same type as the static T, so the kind of logic
    /// error that could occur with the other methods is far less easy to stumble into.
    ///
    /// To define a new event you inherit and
    ///   class MyEventObject : EventObject<MyEventObject>
    ///   {
    ///      ... all data you want
    ///   }
    /// Make an instance
    ///   MyEventObject meo = new MyEventObject();
    ///
    /// To use Listers need to match signature and sub
    ///   void MyHandler(MyEventObject e)
    /// and
    ///   meo.Add(MyHandler);
    /// To send global
    ///   meo.Invoke();
    ///
    /// It intends to make it easy to define events exclusively in terms of the data that they send.
    /// Rather than having to define data to be sent and event as separate classes. Can have instance
    /// listeners or use global listeners.
    ///
    /// NOTE: This paradigm works more cleanly in languages that have more comprehensive or lenient
    /// covariance rules that would make it possible to treat a class that inherits from
    /// EventObject<T> and itself be interchangable throughout the codebase.
    /// </summary>
    public class EventObject<T> where T : class, new()
    {
        public delegate void Handler(T t);

        protected Handler Delegate { get; set; } = null;

        public void Add(Handler handler)
        {
            Delegate += handler;
        }

        public void Remove(Handler handler)
        {
            Delegate -= handler;
        }

        public void StopAllListenting()
        {
            Delegate = null;
        }

        public void Invoke()
        {
            //this cast shouldn't be required
            Delegate?.Invoke(this as T);
        }

        public int ListenerCount
        {
            get
            {
                return Delegate?.GetInvocationList()?.Length ?? 0;
            }
        }
    }
}