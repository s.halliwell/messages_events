﻿using SAE;
using UnityEngine;

namespace Demo
{
    public class EventManagerDemoListen : MonoBehaviour
    {
        private void OnEnable()
        {
            EventManager.Global.Add<EventManagerDemoTypeA>(HandlerA);
            EventManager.Global.Add<EventManagerDemoTypeB>(HandlerB);
        }

        private void OnDisable()
        {
            EventManager.Global.Remove<EventManagerDemoTypeA>(HandlerA);
            EventManager.Global.Remove<EventManagerDemoTypeB>(HandlerB);
        }

        private void HandlerA(EventManagerDemoTypeA param)
        {
            Debug.Log(param.aVal);
        }

        private void HandlerB(EventManagerDemoTypeB param)
        {
            Debug.Log(param.aVal);
        }
    }
}