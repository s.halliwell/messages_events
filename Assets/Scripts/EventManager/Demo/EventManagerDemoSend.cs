﻿using SAE;
using UnityEngine;

namespace Demo
{
    public class EventManagerDemoSend : MonoBehaviour
    {
        public UnityEngine.UI.InputField inputFieldA, inputFieldB;

        public void SendTypeA()
        {
            if (int.TryParse(inputFieldA.text, out int val))
            {
                EventManager.Global.Invoke(new EventManagerDemoTypeA() { aVal = val });
            }
            else
            {
                Debug.LogError("Cannot send TypeA, expects int in text field");
            }
        }

        public void SendTypeB()
        {
            EventManager.Global.Invoke(new EventManagerDemoTypeB() { aVal = inputFieldB.text });
        }
    }
}