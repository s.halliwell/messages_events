﻿using System.Collections.Generic;
using UnityEngine;
using static SAE.EventManager;

namespace SAE
{
    /// <summary>
    /// Helper class for managing 1 type of eventdelegate. The lookups here are only required to fulfil the requirement
    /// of storing many of these in the same collection. This causes a lot of code to be required to hide types and
    /// convert to and from base types safely.
    /// </summary>
    public class EventManagerChannel
    {
        private EventDelegate theDel;
        private Dictionary<System.Delegate, EventDelegate> delegateLookup = new Dictionary<System.Delegate, EventDelegate>();

        public void Add<T>(EventDelegate<T> del) where T : EventManagerData, new()
        {
            EventDelegate internalDel = null;
            delegateLookup.TryGetValue(del, out internalDel);
            if (internalDel != null)
            {
                Debug.LogWarning("Multiple registrations attmpted in " + typeof(T) + ": " + del.ToString() +
                " This is not allowed");
                return;
            }

            // Create a new non-generic delegate which calls our generic one.
            // This is the delegate we actually invoke.
            EventDelegate internalDelegate = (e) => del((T)e);
            delegateLookup[del] = internalDelegate;

            if (theDel == null)
            {
                theDel = internalDelegate;
            }
            else
            {
                theDel += internalDelegate;
            }
        }

        public void Invoke(EventManagerData e)
        {
            if (theDel != null)
                theDel.Invoke(e);
            else
            {
                Debug.LogWarning(e.GetType() + " event raised but has no listeners");
            }
        }

        public void Remove<T>(EventDelegate<T> del) where T : EventManagerData, new()
        {
            EventDelegate internalDel = null;
            delegateLookup.TryGetValue(del, out internalDel);

            if (internalDel != null)
            {
                delegateLookup.Remove(del);
                theDel -= internalDel;
            }
            else
            {
                Debug.LogWarning("Tried to remove delegate that doesnt exist");
            }
        }

        public void RemoveAll()
        {
            delegateLookup.Clear();
            theDel = null;
        }

        public int ListenerCount
        {
            get
            {
                return delegateLookup.Count;
            }
        }
    }
}