﻿using System.Collections.Generic;
using UnityEngine;

namespace SAE
{
    public abstract class EventManagerData
    {
    }

    //originally inspired by http://www.bendangelo.me/unity3d/2014/12/24/unity3d-event-manager.html
    /// <summary>
    /// Unlike the PostOffice, this event system uses types to create different channels. For each new
    /// Event you wish to exist, instead of giving it a string, you make a new type that inherits from EventManagerData.
    /// Then sending and listening via that type is a new event type. This also means every event can have any
    /// amount or types of data sent along with it that it might like.
    ///
    /// This system ends up being a bit cumbersome and slower at runtime than some other alternatives. This
    /// is due to its desire to not define new events just the types of data that passes through them AND
    /// wanting a single access point for all event subscription and invocation, it is that second requirement
    /// that causes most of the supporting and type checking code. In most circumstances that second requirement
    /// is never really that important or may even be a false requirement, it hides something from the internals
    /// of this system but your code that uses it still needs to be aware of those types and work with them.
    /// </summary>
    public class EventManager
    {
        public static EventManager Global = new EventManager();

        public delegate void EventDelegate<T>(T e) where T : EventManagerData, new();

        public delegate void EventDelegate(EventManagerData e);

        protected Dictionary<System.Type, EventManagerChannel> channelLookup = new Dictionary<System.Type, EventManagerChannel>();

        public void Add<T>(EventDelegate<T> del) where T : EventManagerData, new()
        {
            var id = GetChannel<T>(true);

            id.Add(del);
        }

        public void Remove<T>(EventDelegate<T> del) where T : EventManagerData, new()
        {
            EventManagerChannel id = GetChannel<T>(false);
            if (id != null)
            {
                id.Remove(del);
            }
            else
            {
                Debug.LogWarning("Event: " + typeof(T) + " has not been registered");
            }
        }

        public void RemoveAll()
        {
            foreach (var item in channelLookup.Values)
            {
                item.RemoveAll();
            }

            channelLookup.Clear();
        }

        public EventManagerChannel GetChannel<T>(bool createIfNull) where T : EventManagerData, new()
        {
            EventManagerChannel retval = null;
            channelLookup.TryGetValue(typeof(T), out retval);

            if (createIfNull && retval == null)
            {
                retval = new EventManagerChannel();
                //retval.Init<T>();
                channelLookup[typeof(T)] = retval;
            }

            return retval;
        }

        public void Invoke<T>(T e) where T : EventManagerData, new()
        {
            EventManagerChannel id = GetChannel<T>(false);
            if (id != null)
            {
                id.Invoke(e);
            }
            else
            {
                Debug.LogWarning("Event: " + e.GetType() + " has not been registered");
            }
        }

        public int TotalListeners
        {
            get
            {
                int count = 0;

                foreach (var item in channelLookup)
                {
                    count += item.Value.ListenerCount;
                }

                return count;
            }
        }
    }
}