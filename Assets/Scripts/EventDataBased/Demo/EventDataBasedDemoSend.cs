﻿using SAE;
using UnityEngine;

namespace Demo
{
    public class EventDataBasedDemoSend : MonoBehaviour
    {
        public UnityEngine.UI.InputField inputFieldA, inputFieldB;

        public void SendTypeA()
        {
            if (int.TryParse(inputFieldA.text, out int val))
            {
                GlobalEventByParam<EventDataBasedDemoTypeA>.Invoke(new EventDataBasedDemoTypeA() { aVal = val });
            }
            else
            {
                Debug.LogError("Cannot send TypeA, expects int in text field");
            }
        }

        public void SendTypeB()
        {
            GlobalEventByParam<EventDataBasedDemoTypeB>.Invoke(new EventDataBasedDemoTypeB() { aVal = inputFieldB.text });
        }
    }
}