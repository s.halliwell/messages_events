﻿using SAE;
using UnityEngine;

namespace Demo
{
    public class EventDataBasedDemoListen : MonoBehaviour
    {
        private void OnEnable()
        {
            GlobalEventByParam<EventDataBasedDemoTypeA>.Add(HandlerA);
            GlobalEventByParam<EventDataBasedDemoTypeB>.Add(HandlerB);
        }

        private void OnDisable()
        {
            GlobalEventByParam<EventDataBasedDemoTypeA>.Remove(HandlerA);
            GlobalEventByParam<EventDataBasedDemoTypeB>.Remove(HandlerB);
        }

        private void HandlerA(EventDataBasedDemoTypeA param)
        {
            Debug.Log(param.aVal);
        }

        private void HandlerB(EventDataBasedDemoTypeB param)
        {
            Debug.Log(param.aVal);
        }
    }
}