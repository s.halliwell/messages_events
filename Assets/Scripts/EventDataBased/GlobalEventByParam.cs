﻿namespace SAE
{
    /// <summary>
    /// Event Object defined by type or by data type it will send. Similar to the GlobalEvent that
    /// uses delegate to create event channels. By defining in terms of param rather than entire
    /// delegate, we don't require callers to do the hardwaring on the delegate invoke. The possible
    /// benefit here also being that if you desire to pass the object around outside of the
    /// context of the event there is nothing requiring those parties to be aware that the event exist
    /// at all.
    ///
    /// By defining by Param Type we setup a structure where the data defines the category of
    /// the event for listeners to further fitlter from there. A similar setup to Unity's Collision
    /// messages.
    ///
    /// This takes the concepts from GlobalEventByDel but refocuses attention on the information being
    /// sent and received not the detail of the function signature that must be defined to use it.
    ///
    /// First define the data class
    ///   public class MyEventData {public int ival; ...}
    /// Define handlers in terms of the param
    ///   void MyHandler(MyEventData data);
    /// Then interact with via
    ///   GlobalEventByParam<MyEventData>.Add(MyHandler);
    ///
    /// Can be used either by making concrete class or in the generic.
    /// </summary>
    public static class GlobalEventByParam<T>
    {
        public delegate void EventParamHandlerDelegate(T arg);

        private static EventParamHandlerDelegate Delegate { get; set; } = null;

        public static void Add(EventParamHandlerDelegate del)
        {
            Delegate += del;
        }

        public static void Remove(EventParamHandlerDelegate del)
        {
            Delegate -= del;
        }

        public static void Invoke(T arg)
        {
            Delegate?.Invoke(arg);
        }

        public static void Clear()
        {
            Delegate = null;
        }

        public static int ListenerCount
        {
            get
            {
                return Delegate?.GetInvocationList()?.Length ?? 0;
            }
        }
    }
}