﻿using NUnit.Framework;

namespace Tests
{
    public class PostOfficeTests
    {
        private int globalIntVal;

        private string globalStringVal;

        [Test]
        public void AddRemoveListener()
        {
            Assert.That(SAE.PostOffice.Global.CalculateTotalListeners() == 0);
            Assert.That(SAE.PostOffice.Global.CalculateMajorListeners("Test") == 0);
            Assert.That(SAE.PostOffice.Global.CalculateMinorListeners("Test", string.Empty) == 0);
            SAE.PostOffice.Global.Listen(AddRemoveListenerHandler, "Test", string.Empty);
            Assert.That(SAE.PostOffice.Global.CalculateTotalListeners() == 1);
            SAE.PostOffice.Global.Listen(AddRemoveListenerHandler, "Test", string.Empty);
            Assert.That(SAE.PostOffice.Global.CalculateTotalListeners() == 2);
            Assert.That(SAE.PostOffice.Global.CalculateMajorListeners("NotTest") == 0);
            Assert.That(SAE.PostOffice.Global.CalculateMajorListeners("Test") == 2);
            Assert.That(SAE.PostOffice.Global.CalculateMinorListeners("Test", "None") == 0);
            Assert.That(SAE.PostOffice.Global.CalculateMinorListeners("Test", string.Empty) == 2);
            SAE.PostOffice.Global.StopListening(AddRemoveListenerHandler, "Test", string.Empty);
            Assert.That(SAE.PostOffice.Global.CalculateTotalListeners() == 1);
            SAE.PostOffice.Global.StopListeningAll(AddRemoveListenerHandler);
            Assert.That(SAE.PostOffice.Global.CalculateTotalListeners() == 0);
            Assert.That(SAE.PostOffice.Global.CalculateMajorListeners("Test") == 0);
            Assert.That(SAE.PostOffice.Global.CalculateMinorListeners("Test", string.Empty) == 0);
        }

        [Test]
        public void GlobalPost()
        {
            globalIntVal = 0;

            var localPost = new SAE.PostOffice();

            SAE.PostOffice.Global.Listen(SimpleIncrementMinorHandler, "Test", "Inc1");
            SAE.PostOffice.Global.Listen(SimpleIncrementMinorHandler, "Test", "Inc2");
            SAE.PostOffice.Global.Listen(SimpleIncrementMinorHandler, "Test", "Inc3");

            SAE.PostOffice.Global.Send("Test", "Inc1");
            SAE.PostOffice.Global.Send("Test", "Inc2");
            SAE.PostOffice.Global.Send("Test", "Inc3");

            Assert.That(globalIntVal == 3);
            SAE.PostOffice.Global.StopListeningAll(SimpleIncrementMinorHandler);
        }

        [Test]
        public void Listen()
        {
            globalStringVal = "Fails";

            SAE.PostOffice.Global.Listen(SimpleAssignMinorHandler, "Test", "Pass");

            SAE.PostOffice.Global.Send("Test", "Pass");

            Assert.That(globalStringVal == "Pass");

            SAE.PostOffice.Global.StopListeningAll(SimpleAssignMinorHandler);
        }

        [Test]
        public void LocalPost()
        {
            globalIntVal = 0;

            var localPost = new SAE.PostOffice();

            localPost.Listen(SimpleIncrementMinorHandler, "Test", "Inc1");
            localPost.Listen(SimpleIncrementMinorHandler, "Test", "Inc2");
            localPost.Listen(SimpleIncrementMinorHandler, "Test", "Inc3");

            localPost.Send("Test", "Inc1");
            localPost.Send("Test", "Inc2");
            localPost.Send("Test", "Inc3");

            Assert.That(globalIntVal == 3);
        }

        [Test]
        public void LocalPostAny()
        {
            globalIntVal = 0;

            var localPost = new SAE.PostOffice();

            localPost.Listen(SimpleIncrementMinorHandler, "Test", "");

            localPost.Send("Test", "Inc1");
            localPost.Send("Test", "Inc2");
            localPost.Send("Test", "");

            Assert.That(globalIntVal == 3);
        }

        [Test]
        public void RemoveAll()
        {
            globalStringVal = "Initial";

            SAE.PostOffice.Global.Listen(SimpleAssignMinorHandler, "Test", "Pass");
            SAE.PostOffice.Global.RemoveAllListeners();

            SAE.PostOffice.Global.Send("Test", "Pass");

            Assert.That(globalStringVal == "Initial");
        }

        [Test]
        public void RemoveAllMajor()
        {
            globalStringVal = "Initial";

            SAE.PostOffice.Global.Listen(SimpleAssignMinorHandler, "Test", "Pass");
            SAE.PostOffice.Global.RemoveAllMajorListeners("Nothing");

            SAE.PostOffice.Global.Send("Test", "Pass");
            Assert.That(globalStringVal == "Pass");
            globalStringVal = "Initial";

            SAE.PostOffice.Global.RemoveAllMajorListeners("Test");

            SAE.PostOffice.Global.Send("Test", "Pass");

            Assert.That(globalStringVal == "Initial");
        }

        [Test]
        public void RemoveAllMinor()
        {
            globalStringVal = "Initial";

            SAE.PostOffice.Global.Listen(SimpleAssignMinorHandler, "Test", "Pass");
            SAE.PostOffice.Global.RemoveAllMinorListeners("Nothing", "Pass");

            SAE.PostOffice.Global.Send("Test", "Pass");
            Assert.That(globalStringVal == "Pass");
            globalStringVal = "Initial";

            SAE.PostOffice.Global.RemoveAllMinorListeners("Test", "Pass");

            SAE.PostOffice.Global.Send("Test", "Pass");

            Assert.That(globalStringVal == "Initial");
        }

        [Test]
        public void StopListen()
        {
            globalStringVal = "Initial";

            SAE.PostOffice.Global.Listen(SimpleAssignMinorHandler, "Test", "Pass");
            SAE.PostOffice.Global.StopListening(SimpleAssignMinorHandler, "Test", "Pass");

            SAE.PostOffice.Global.Send("Test", "Pass");

            Assert.That(globalStringVal == "Initial");

            SAE.PostOffice.Global.StopListeningAll(SimpleAssignMinorHandler);
        }

        [Test]
        public void StopListenAll()
        {
            globalStringVal = "Initial";

            SAE.PostOffice.Global.Listen(SimpleAssignMinorHandler, "Test", "Pass");
            SAE.PostOffice.Global.StopListeningAll(SimpleAssignMinorHandler);

            SAE.PostOffice.Global.Send("Test", "Pass");

            Assert.That(globalStringVal == "Initial");
        }

        private void AddRemoveListenerHandler(string maj, string min)
        {
        }

        private void SimpleAssignMinorHandler(string maj, string min)
        {
            globalStringVal = min;
        }

        private void SimpleIncrementMinorHandler(string maj, string min)
        {
            globalIntVal++;
        }
    }
}