﻿using NUnit.Framework;

namespace Tests
{
    public class EventObjectTests
    {
        internal class TestEventObject : SAE.EventObject<TestEventObject>
        {
            public int intVal;
        }

        internal class AnotherTestEventObject : SAE.EventObject<AnotherTestEventObject>
        {
        }

        private int globalIntVal;

        static internal TestEventObject teo = new TestEventObject();
        static internal AnotherTestEventObject ateo = new AnotherTestEventObject();

        [Test]
        public void AddRemoveListenerLocal()
        {
            var teo = new TestEventObject();

            Assert.That(teo.ListenerCount == 0);
            teo.Add(TestEventObjectHandler);
            Assert.That(teo.ListenerCount == 1);
            teo.Add(TestEventObjectHandler);
            Assert.That(teo.ListenerCount == 2);
            teo.Remove(TestEventObjectHandler);
            Assert.That(teo.ListenerCount == 1);
            teo.StopAllListenting();
            Assert.That(teo.ListenerCount == 0);
        }

        [Test]
        public void AddRemoveListenerGlobal()
        {
            Assert.That(teo.ListenerCount == 0);
            teo.Add(TestEventObjectHandler);
            Assert.That(teo.ListenerCount == 1);
            teo.Add(TestEventObjectHandler);
            Assert.That(teo.ListenerCount == 2);
            Assert.That(ateo.ListenerCount == 0);
            teo.Remove(TestEventObjectHandler);
            Assert.That(teo.ListenerCount == 1);
            teo.StopAllListenting();
            Assert.That(teo.ListenerCount == 0);
        }

        [Test]
        public void GlobalPost()
        {
            globalIntVal = 0;
            teo.Add(TestEventObjectHandler);

            teo.intVal = 5;
            teo.Invoke();

            teo.StopAllListenting();
            Assert.That(globalIntVal == 5);
        }

        [Test]
        public void LocalPost()
        {
            globalIntVal = 0;
            var teo = new TestEventObject() { intVal = 5 };
            teo.Add(TestEventObjectHandler);
            teo.Invoke();

            Assert.That(globalIntVal == 5);
        }

        private void TestEventObjectHandler(TestEventObject testEventObject)
        {
            globalIntVal += testEventObject.intVal;
        }
    }
}