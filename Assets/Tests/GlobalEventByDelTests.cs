﻿using NUnit.Framework;

namespace Tests
{
    public class GlobalEventByDelTests
    {
        private int globalIntVal;

        private delegate void TestDelegate(int aNum);

        private delegate void ADifferentTestDelegate(int aNum);

        [Test]
        public void AddRemoveListener()
        {
            SAE.GlobalEventByDel<TestDelegate>.Add(SimpleTestDelegate);
            Assert.That(SAE.GlobalEventByDel<TestDelegate>.ListenerCount == 1);
            SAE.GlobalEventByDel<ADifferentTestDelegate>.Add(SimpleTestDelegate);
            Assert.That(SAE.GlobalEventByDel<TestDelegate>.ListenerCount == 1);
            SAE.GlobalEventByDel<TestDelegate>.Add(AnotherSimpleTestDelegate);
            Assert.That(SAE.GlobalEventByDel<TestDelegate>.ListenerCount == 2);
            SAE.GlobalEventByDel<TestDelegate>.Remove(SimpleTestDelegate);
            Assert.That(SAE.GlobalEventByDel<TestDelegate>.ListenerCount == 1);
            SAE.GlobalEventByDel<TestDelegate>.Clear();
            Assert.That(SAE.GlobalEventByDel<TestDelegate>.ListenerCount == 0);
            SAE.GlobalEventByDel<ADifferentTestDelegate>.Clear();
        }

        [Test]
        public void GlobalPost()
        {
            globalIntVal = 0;
            SAE.GlobalEventByDel<TestDelegate>.Add(SimpleTestDelegate);
            SAE.GlobalEventByDel<TestDelegate>.Delegate?.Invoke(5);
            SAE.GlobalEventByDel<TestDelegate>.Delegate?.Invoke(5);

            Assert.That(globalIntVal == 10);
            SAE.GlobalEventByDel<TestDelegate>.Clear();
        }

        private void SimpleTestDelegate(int i)
        {
            globalIntVal += i;
        }

        private void AnotherSimpleTestDelegate(int i)
        {
        }
    }
}