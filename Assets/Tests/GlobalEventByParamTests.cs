﻿using NUnit.Framework;
using SAE;

namespace Tests
{
    public class GlobalEventByParamTests
    {
        public class EventParamData
        {
            public int intVal;
        }

        private int globalIntVal;

        [Test]
        public void AddRemoveListenerGlobal()
        {
            int startingCount = GlobalEventByParam<EventParamData>.ListenerCount;

            Assert.That(GlobalEventByParam<EventParamData>.ListenerCount == startingCount + 0);
            GlobalEventByParam<EventParamData>.Add(TestEventObjectHandler);
            Assert.That(GlobalEventByParam<EventParamData>.ListenerCount == startingCount + 1);
            GlobalEventByParam<EventParamData>.Add(TestEventObjectHandler);
            Assert.That(GlobalEventByParam<EventParamData>.ListenerCount == startingCount + 2);
            GlobalEventByParam<EventParamData>.Remove(TestEventObjectHandler);
            Assert.That(GlobalEventByParam<EventParamData>.ListenerCount == startingCount + 1);
            GlobalEventByParam<EventParamData>.Clear();
            Assert.That(GlobalEventByParam<EventParamData>.ListenerCount == startingCount + 0);
        }

        [Test]
        public void Post()
        {
            globalIntVal = 0;
            GlobalEventByParam<EventParamData>.Add(TestEventObjectHandler);
            GlobalEventByParam<EventParamData>.Invoke(new EventParamData() { intVal = 5 });
            Assert.That(globalIntVal == 5);
            GlobalEventByParam<EventParamData>.Clear();
        }

        private void TestEventObjectHandler(EventParamData eventParamData)
        {
            globalIntVal += eventParamData.intVal;
        }
    }
}