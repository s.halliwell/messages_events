﻿using NUnit.Framework;

namespace Tests
{
    public class EventManagerTests
    {
        internal class TestEventData : SAE.EventManagerData
        {
            internal int intval;
        }

        internal class NothingEventData : SAE.EventManagerData { }

        private int globalIntVal;

        [Test]
        public void AddRemoveListener()
        {
            Assert.That(SAE.EventManager.Global.TotalListeners == 0);
            Assert.That(SAE.EventManager.Global.GetChannel<TestEventData>(true).ListenerCount == 0);
            SAE.EventManager.Global.Add<TestEventData>(SimpleHandler1);
            Assert.That(SAE.EventManager.Global.TotalListeners == 1);
            Assert.That(SAE.EventManager.Global.GetChannel<TestEventData>(true).ListenerCount == 1);
            Assert.That(SAE.EventManager.Global.GetChannel<NothingEventData>(true).ListenerCount == 0);
            //no dups so check that
            SAE.EventManager.Global.Add<TestEventData>(SimpleHandler1);
            Assert.That(SAE.EventManager.Global.TotalListeners == 1);
            Assert.That(SAE.EventManager.Global.GetChannel<TestEventData>(true).ListenerCount == 1);
            Assert.That(SAE.EventManager.Global.GetChannel<NothingEventData>(true).ListenerCount == 0);

            SAE.EventManager.Global.Add<TestEventData>(SimpleHandler2);
            Assert.That(SAE.EventManager.Global.TotalListeners == 2);
            Assert.That(SAE.EventManager.Global.GetChannel<TestEventData>(true).ListenerCount == 2);
            Assert.That(SAE.EventManager.Global.GetChannel<NothingEventData>(true).ListenerCount == 0);

            SAE.EventManager.Global.Remove<TestEventData>(SimpleHandler1);
            Assert.That(SAE.EventManager.Global.TotalListeners == 1);
            Assert.That(SAE.EventManager.Global.GetChannel<TestEventData>(true).ListenerCount == 1);
            Assert.That(SAE.EventManager.Global.GetChannel<NothingEventData>(true).ListenerCount == 0);

            SAE.EventManager.Global.GetChannel<TestEventData>(true).RemoveAll();
            Assert.That(SAE.EventManager.Global.TotalListeners == 0);
            Assert.That(SAE.EventManager.Global.GetChannel<TestEventData>(true).ListenerCount == 0);
            Assert.That(SAE.EventManager.Global.GetChannel<NothingEventData>(true).ListenerCount == 0);
        }

        [Test]
        public void GlobalPost()
        {
            globalIntVal = 0;

            SAE.EventManager.Global.Add<TestEventData>(AccumulateTestEventDataHandler);
            var ted = new TestEventData() { intval = 5 };
            SAE.EventManager.Global.Invoke(ted);

            Assert.That(globalIntVal == 5);

            SAE.EventManager.Global.RemoveAll();
        }

        [Test]
        public void LocalPost()
        {
            globalIntVal = 0;

            var localEM = new SAE.EventManager();

            localEM.Add<TestEventData>(AccumulateTestEventDataHandler);
            var ted = new TestEventData() { intval = 5 };
            localEM.Invoke(ted);

            Assert.That(globalIntVal == 5);

            localEM.RemoveAll();
        }

        private void SimpleHandler1(TestEventData testEventData)
        {
        }

        private void SimpleHandler2(TestEventData testEventData)
        {
        }

        private void AccumulateTestEventDataHandler(TestEventData testEventData)
        {
            globalIntVal += testEventData.intval;
        }
    }
}